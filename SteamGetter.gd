extends Control

var defaultpos = Rect2(0, 0, 0, 0);

var peaks = null;
var installed = false;
var downloadedLatestDone = false;
var deleting = false;

# Called when the node enters the scene tree for the first time.
func _ready():
	defaultpos = Rect2(get_tree().get_root().position, get_tree().get_root().size);
	
	$Button.visible = false;
	$ProgressBar.visible = false;
	var result = getSteamDir();
	#result = null;
	
	if result == null:
		$Label.text = "Could not find Peaks of Yore. You either didn't install Steam to the default location, don't have Peaks of Yore installed, or pirated the game";
		$Button2.visible = true;
	elif result[0] == 0:
		$Label.text = result[1];
		$Button.visible = true;
	elif result[0] == 1:
		$Label.text = result[1];
		$Button3.visible = true;
		$Button.visible = true;
		
		$Button.size.x = 240;
		$Button.position.x = 240;
		$Button.text = "Update and/or Reinstall\nBag With Friends";
		
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$FileDialog.size = get_tree().get_root().size;
	$FileDialog.size.y -= 32;
	
	if installed:
		if Input.is_anything_pressed():
			get_tree().quit();
	pass


func getSteamDir():
	if !FileAccess.file_exists("C:\\Program Files (x86)\\Steam\\config\\libraryfolders.vdf"):
		return null;
		
	var file = FileAccess.open("C:\\Program Files (x86)\\Steam\\config\\libraryfolders.vdf", FileAccess.READ);
	var locations = file.get_as_text().split("{");
	var totalLocations = (locations.size() - 2) / 2;
	print(String.num_int64(totalLocations) + " Steam library folders");
	var folders = [];
	
	for i in range(2, locations.size(), 2):
		var folder = locations[i].split("\"")[3] + "\\steamapps\\common"
		folders.push_back(folder);
		print(folder);
		
		if DirAccess.dir_exists_absolute(folder + "\\Peaks of Yore"):
			folder = folder.replace("\\\\", "\\");
			peaks = folder + "\\Peaks of Yore";
			print("Found Peaks of Yore at " + folder + "\\Peaks of Yore");
			
			if FileAccess.file_exists(folder + "\\Peaks of Yore\\Mods\\Bag With Friends.dll"):
				return [1, "Found Bag With Friends at " + folder + "\\Peaks of Yore"];
				
			return [0, "Found Peaks of Yore at " + folder + "\\Peaks of Yore"];
		


func _on_button_pressed():
	$ProgressBar.visible = true;
	
	var downloadLastest = HTTPRequest.new();
	add_child(downloadLastest);
	downloadLastest.download_file = peaks + "\\" + "Bag_With_Friends_Latest.zip";
	downloadLastest.request_completed.connect(doneDownloadLatest.bind(self));
	downloadLastest.request("https://bwf.givo.xyz/Bag_With_Friends/Bag_With_Friends.zip");
	
	while !downloadedLatestDone:
		$ProgressBar.value = 100;
		$ProgressBar/Label.text = "" if !deleting else "Getting file list\n";
		$ProgressBar/Label.text += str(downloadLastest.get_downloaded_bytes()) + "/? bytes\ndownloaded";
		await get_tree().create_timer(0.01).timeout;
	
	var zip := ZIPReader.new();
	var err := zip.open("res://Zip.zip");
	if err != OK:
		print("error");
		return PackedByteArray();
	
	var files = zip.get_files();
	var fileIndex = 0;
	
	for file in files:
		var noZip = file.erase(0, 4);
		if (noZip.ends_with("/") && !deleting) || noZip == "":
			DirAccess.make_dir_absolute(peaks + "\\" + noZip);
			fileIndex += 1;
			continue;
		
		if !deleting:
			var writeFile = FileAccess.open(peaks + "\\" + noZip, FileAccess.WRITE);
			var readFile = zip.read_file(file);
			writeFile.store_buffer(readFile);
			writeFile.close();
		else:
			DirAccess.remove_absolute(peaks + "\\" + noZip);
		
		fileIndex += 1;
		$ProgressBar.value = ((fileIndex + 0.0) / (files.size() + 0.0)) * 100.0;
		$ProgressBar/Label.text = noZip;
		await get_tree().create_timer(0.01).timeout;
		
	zip.close();
	
	zip = ZIPReader.new();
	err = zip.open(peaks + "\\" + "Bag_With_Friends_Latest.zip");
	if err != OK:
		print("error");
		return PackedByteArray();
	
	files = zip.get_files();
	fileIndex = 0;
	
	for file in files:
		var noZip = file;
		
		if noZip.find("Installation") != -1:
			continue;
		
		if (noZip.ends_with("/") && !deleting) || noZip == "":
			DirAccess.make_dir_absolute(peaks + "\\" + noZip);
			fileIndex += 1;
			continue;
			
		if !deleting:
			var writeFile = FileAccess.open(peaks + "\\" + noZip, FileAccess.WRITE);
			var readFile = zip.read_file(file);
			writeFile.store_buffer(readFile);
			writeFile.close();
		else:
			DirAccess.remove_absolute(peaks + "\\" + noZip);
		
		fileIndex += 1;
		$ProgressBar.value = ((fileIndex + 0.0) / (files.size() + 0.0)) * 100.0;
		$ProgressBar/Label.text = noZip;
		await get_tree().create_timer(0.01).timeout;
		
	zip.close();
	
	installed = true;
	$ProgressBar.value = 100;
	
	if !deleting:
		$ProgressBar/Label.text = "Bag With Friends install complete!\nYou can close this window or press any key";
	else:
		$ProgressBar/Label.text = "Bag With Friends uninstall complete!\nYou can close this window or press any key";

func _on_button_2_pressed():
	#$ColorRect.visible = false;
	#$Label.visible = false;
	$Button2.visible = false;
	#get_tree().get_root().position = DisplayServer.screen_get_usable_rect(DisplayServer.get_primary_screen()).position;
	#get_tree().get_root().size = DisplayServer.screen_get_usable_rect(DisplayServer.get_primary_screen()).size;
	
	#print(get_tree().get_root().size);
	$FileDialog.visible = true;
	get_tree().get_root().unresizable = false;
	#$FileDialog.position = defaultpos.position;

func _on_file_dialog_dir_selected(dir):
	get_tree().get_root().unresizable = true;
	get_tree().get_root().size = defaultpos.size;
	print(dir);
	#get_tree().get_root().position = defaultpos.position;
	#get_tree().get_root().size = defaultpos.size;
	
	$ColorRect.visible = true;
	$Label.visible = true;
	peaks = dir;
	$Label.text = "Folder selected: " + dir;
	$Button.visible = true;
	pass # Replace with function body.


func _on_file_dialog_canceled():
	$Button2.visible = true;
	get_tree().get_root().unresizable = true;
	get_tree().get_root().size = defaultpos.size;
	pass # Replace with function body.

func doneDownloadLatest(a1, a2, a3, a4, a5):
	downloadedLatestDone = true;


func _on_button_3_pressed():
	deleting = true;
	_on_button_pressed();
	pass # Replace with function body.
